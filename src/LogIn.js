import React, { Component } from 'react';
import { View, Dimensions, Text, StyleSheet, ImageBackground, SafeAreaView, TextInput, TouchableOpacity} from 'react-native';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const LogIn = ({ navigation }) => {
        return (
        
            <SafeAreaView>
                <View style={styles.signinText}>
                    <Text style={{
                        fontSize: 40,
                        paddingVertical:30,
                        alignItems: 'center',
                         justifyContent: 'center',
                         }}>Hello, Welcom back</Text>
                    <Text>Please SingIn to your Account</Text>
                </View>
        <View style={styles.form}>
        <TextInput
         style={styles.input}
        placeholder="Email"
        />

        <TextInput 
        style={styles.input}
        placeholder="Password"
        
        />
        </View>
        
        <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Dashboard')}
        >
        <Text style={{color: 'white'}}>LogIn</Text>
        </TouchableOpacity>

     </SafeAreaView>
            
    );
};

// syle propeties
const styles = StyleSheet.create({
    container: {
    flex: 3,
    // paddingVertical: 60,
    alignItems: 'center',
    justifyContent: 'center'
    },
    input: {
        // height: 10,
        margin: 20,
        borderWidth: 1,
        // borderBottomWidth: 3,
        borderRadius: 10,
        padding: 10,
        borderBottomColor: 'tael'
    },
    button: {
        alignItems: "center",
        backgroundColor: 'teal',
        padding: 10,
        margin:50,
        borderRadius: 20,
    },
   form: {
        margin:16,
        paddingTop: windowHeight/3
   },
   signinText: {
    alignItems: 'center',
    justifyContent: 'center',
    // padding: 20,
   }
});


export default LogIn;