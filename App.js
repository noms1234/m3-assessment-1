
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import LogIn from './src/Login';
import Dashboard from './src/Dashboard';
import AddItem from './src/AddItem';




const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="login"
          component={LogIn}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Dashboard" component={Dashboard} options={{title: 'Dashboard'}}/>
        <Stack.Screen name="AddItem" component={AddItem} options={{title: 'Add to cart'}}/>
      
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});